DROP TABLE IF EXISTS countries;

CREATE TABLE countries (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  name VARCHAR(250) NOT NULL,
  current_president VARCHAR(250) NOT NULL,
  currency varchar2(3) NOT NULL,
  code number not null,
  alternate_name varchar(250) not null
);

INSERT INTO countries (name, current_president, currency, code, alternate_name) VALUES
  ('United States', 'Biden', 'USD', 1, 'USA'),
  ('Colombia', 'Duque', 'COP', 57, 'LOCOMBIA'),
  ('Canada', 'Trudeau', 'CAD', 1, 'COLD');
